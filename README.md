## Install Instructions

CMake 3.x is required. You can install it via the following ppa:

    sudo add-apt-repository ppa:george-edison55/cmake-3.x
    sudo apt-get update
    sudo apt-get install cmake

Use [catkin tools](https://catkin-tools.readthedocs.org/en/latest/) to install package:

Create a catkin workspace if needed

    mkdir -p my_ws/src
    cd my_ws
    catkin config --init --mkdirs --extend /opt/ros/indigo \
    --cmake-args -DCMAKE_BUILD_TYPE=Release

Clone dependencies:

    cd src
    git clone git@github.com:zurich-eye/cmake_external_project_catkin.git
    git clone git@github.com:zurich-eye/catkin_simple.git
    git clone git@github.com:zurich-eye/glog_catkin.git
    git clone git@github.com:zurich-eye/gflags_catkin.git
    git clone git@github.com:zurich-eye/eigen_catkin.git
    git clone git@github.com:ethz-asl/opengv.git
    git clone https://bitbucket.org/gtborg/gtsam.git
    git clone git@github.com:zurich-eye/gtsam_catkin.git
    git clone https://bitbucket.org/cforster/duckie_vio.git

Build

    catkin build


## Run Instructions

Download dataset from:

    http://projects.asl.ethz.ch/datasets/doku.php?id=kmavvisualinertialdatasets

Download, e.g. Rosbag from Machine Hall 01:

    http://robotics.ethz.ch/~asl-datasets/ijrr_euroc_mav_dataset/machine_hall/MH_01_easy/MH_01_easy.bag

Run:

    rosrun duckie_vio duckie_vio_node -v=40 -skip_frames=730 -vio_ransac_reprojection_err_thresh=0.05 -bag_filename=/data/ze_datasets/euroc_machine_hall/MH_01_easy.bag -vio_keyframe_rate=4 -vio_smart_reprojection_err_thresh=2.0 -vio_viz_sleep=1 -vio_max_landmarks_per_frame=200
