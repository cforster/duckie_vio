#pragma once

#include <memory>
#include <unordered_map>

#include <opencv2/core/core.hpp>
#include <duckie_vio/vio_types.h>
#include <duckie_vio/vio_imu_buffer.h>

#include <gtsam/geometry/Cal3DS2.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/ISAM2.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/slam/SmartProjectionPoseFactor.h>

namespace duckie {

// fwd
class Visualizer;

class Vio
{
public:
  using SmartFactor = gtsam::SmartProjectionPoseFactor<gtsam::Cal3_S2>;
  using PreintegratedImuMeasurements = gtsam::PreintegratedCombinedMeasurements;
  using PreintegratedImuMeasurementPtr = std::shared_ptr<PreintegratedImuMeasurements>;
  using LandmarkIdSmartFactorMap = std::unordered_map<LandmarkId, SmartFactor::shared_ptr>;
  using SmartFactorMap = gtsam::FastMap<LandmarkId, std::pair<SmartFactor::shared_ptr, int>>;

  Vio(const boost::shared_ptr<gtsam::Cal3DS2>& cam,
      const Pose3& T_B_C);

  void addInertialMeasurement(
      const int64_t timestamp, const Vector3& accel, const Vector3& gyro);

  void processSynchronizedData(
      const int64_t timestamp, const cv::Mat& img,
      const ImuStamps& imu_stamps, const ImuAccGyr& imu_accgyr);

  void synchronizeCameraImuAndProcessWhenReady();

  void addImage(
      const int64_t timestamp, const cv::Mat& img);

  void featureTracking(
      Frame& ref_frame, Frame& cur_frame);

  void featureDetection(
      const Frame* ref_frame, Frame& cur_frame);

  std::vector<std::pair<size_t, size_t>> findMatchingKeypoints(
      const Frame& ref_frame, const Frame& cur_frame);

  double computeMedianDisparity(
      const Frame& ref_frame, const Frame& cur_frame);

  bool geometricOutlierRejection(
      Frame& ref_frame, Frame& cur_frame,
      const Rot3& R_ref_cur);

  void addLandmarksToGraph(
      const Frame& frame);

  void addLandmarkToGraph(
      LandmarkId lm_id, Landmark& lm);

  void updateLandmarkInGraph(
      const LandmarkId lm_id, const Landmark& lm,
      const std::pair<FrameId, Point2>& obs);

  void integrateImuMeasurements(
      const ImuStamps& imu_stamps, const ImuAccGyr& imu_accgyr);

  void addImuFactor(
      const FrameId& from_id, const FrameId& to_id);

  void addInertialPriorFactors(
      const FrameId& frame_id,
      const ImuAccGyr& imu_accgyr);

  void addZeroVelocityPrior(
      const FrameId& frame_id);

  void addNoMotionFactor(
      const FrameId& from_id, const FrameId& to_id);

  void optimize(int max_iterations);

  std::shared_ptr<Visualizer> viz_;
  boost::shared_ptr<gtsam::Cal3DS2> cam_;
  boost::shared_ptr<gtsam::Cal3_S2> unitcam_;
  Pose3 T_B_C_; // Camera-Body/Imu transformation.
  PreintegratedImuMeasurementPtr pim_;


  // Data buffering & synchronization:
  ImuBuffer imu_buffer_;
  int64_t new_img_stamp_ = 0;
  int64_t last_img_stamp_ = 0;
  cv::Mat new_img_;

  // Past frames:
  std::shared_ptr<Frame> frame_k_;      //!< Newest frame at time k
  std::shared_ptr<Frame> frame_km1_;    //!< Last frame at time   k-1
  std::shared_ptr<Frame> frame_lkf_;    //!< Last key-frame (lkf).
  ImuBias imu_bias_lkf_;                //!< Most recent bias estimate..
  Vector3 W_v_Blkf_ = Vector3::Zero();  //!< Velocity of body at k-1 in world coordinates
  Pose3   T_W_Blkf_;                    //!< Body pose at at k-1 in world coordinates.

  // Data:
  LandmarkTable landmarks_;

  // Counters:
  int frame_count_ = 0;
  int landmark_count_ = 1;

  // GTSAM:
  std::shared_ptr<gtsam::ISAM2> isam_;
  gtsam::Values state_;                        //!< current state of the system.
  gtsam::NonlinearFactorGraph new_factors_;    //!< new factors to be added
  gtsam::Values new_values_;                   //!< new states to be added
  LandmarkIdSmartFactorMap new_smart_factors_; //!< pointId -> {SmartFactorPtr}
  SmartFactorMap smart_factors_map_;           //!< pointId -> {SmartFactorPtr, SlotIndex}

  // Noise models:
  boost::shared_ptr<PreintegratedImuMeasurements::Params> preintegration_params_;
  std::vector<gtsam::SharedNoiseModel> px_noise_pyr_;
  gtsam::SharedNoiseModel smart_noise_;
  gtsam::SmartProjectionParams smart_factor_params_;
};


} // namespace duckie
