#pragma once

#include <map>
#include <tuple>
#include <thread>
#include <utility>
#include <mutex>
#include <Eigen/Dense>
#include <glog/logging.h>

#include <duckie_vio/vio_types.h>

namespace duckie {

class ImuBuffer
{
public:
  using Data = std::map<int64_t, Vector6, std::less<int64_t>,
                        Eigen::aligned_allocator<Vector6>>;

  ImuBuffer() = default;
  ImuBuffer(double buffer_size_seconds)
    : buffer_size_nanosec_(buffer_size_seconds * 1e9)
  {}

  inline void insert(int64_t stamp, const Vector6& acc_gyr)
  {
    std::lock_guard<std::mutex> lock(mutex_);
    buffer_[stamp] = acc_gyr;
    if(buffer_size_nanosec_ > 0)
    {
      removeDataBeforeTimestamp_impl(
            buffer_.rbegin()->first - buffer_size_nanosec_);
    }
  }

  //! Get timestamps of newest and oldest entry.
  std::tuple<int64_t, int64_t, bool> getOldestAndNewestStamp() const;

  /*! @brief Get Values between timestamps.
   *
   * If timestamps are not matched, the values
   * are interpolated. Returns a vector of timestamps and a block matrix with
   * values as columns. Returns empty matrices if not successful.
   */
  std::pair<ImuStamps, ImuAccGyr>
  getBetweenValuesInterpolated(int64_t stamp_from, int64_t stamp_to);

  inline void clear()
  {
    std::lock_guard<std::mutex> lock(mutex_);
    buffer_.clear();
  }

  inline size_t size() const
  {
    std::lock_guard<std::mutex> lock(mutex_);
    return buffer_.size();
  }

  inline bool empty() const
  {
    std::lock_guard<std::mutex> lock(mutex_);
    return buffer_.empty();
  }

  inline void removeDataBeforeTimestamp(int64_t stamp)
  {
    std::lock_guard<std::mutex> lock(mutex_);
    removeDataBeforeTimestamp_impl(stamp);
  }

  inline void lock() const
  {
    mutex_.lock();
  }

  inline void unlock() const
  {
    mutex_.unlock();
  }

  const Data& data() const
  {
    CHECK(!mutex_.try_lock()) << "Call lock() before accessing data.";
    return buffer_;
  }

  typename Data::iterator iterator_equal_or_before(int64_t stamp);

  typename Data::iterator iterator_equal_or_after(int64_t stamp);

protected:
  mutable std::mutex mutex_;
  Data buffer_;
  int64_t buffer_size_nanosec_ = -1; // Negative means, no fixed size.

  inline void removeDataBeforeTimestamp_impl(int64_t stamp)
  {
    auto it = buffer_.lower_bound(stamp);
    buffer_.erase(buffer_.begin(), it);
  }
};

} // namespace duckie
