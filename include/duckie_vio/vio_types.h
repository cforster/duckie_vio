#pragma once

#include <cstdint>
#include <vector>
#include <unordered_map>

#include <Eigen/Core>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Point2.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/navigation/ImuBias.h>
#include <opencv2/core/core.hpp>

namespace duckie {

// Scalars
using size_t  = std::size_t;
using int64_t = std::int64_t;
using uint8_t = std::uint8_t;

// Typedefs of commonly used Eigen matrices and vectors.
using Vector3 = Eigen::Vector3d;
using Vector6 = Eigen::Matrix<double, 6, 1>;
using Matrix3 = Eigen::Matrix3d;
using Matrix6 = Eigen::Matrix<double, 6, 6>;

// Inertial containers.
using ImuStamps = Eigen::Matrix<int64_t, Eigen::Dynamic, 1>;
using ImuAccGyr = Eigen::Matrix<double, 6, Eigen::Dynamic>;

// Gtsam types.
using gtsam::Pose3;
using gtsam::Rot3;
using gtsam::Point3;
using gtsam::Point2;
using gtsam::Key;
using ImuBias = gtsam::imuBias::ConstantBias;

// Common VIO types.
using FrameId = int;
using LandmarkId = int;
using BearingVectors = std::vector<Vector3, Eigen::aligned_allocator<Vector3>>;
using Keypoints = std::vector<Point2>;
using LandmarkIds = std::vector<LandmarkId>;

//! Frame contains the image and the keypoints with landmark references.
class Frame
{
public:
  using Ptr = std::shared_ptr<Frame>;

  const FrameId id_;
  const int64_t timestamp_;
  const cv::Mat img_;
  bool is_keyframe_ = false;

  // These containers must have same size.
  Keypoints keypoints_;
  LandmarkIds landmarks_;

  Frame() = delete;   // No default constructor.
  ~Frame() = default;

  Frame(const FrameId id, const int64_t timestamp, const cv::Mat& img)
    : id_(id), timestamp_(timestamp), img_(img)
  {}

  // No copy.
  Frame(const Frame&) = delete;
  Frame& operator=(const Frame&) = delete;
};

//! Landmark
class Landmark
{
public:
  std::vector<std::pair<FrameId, Point2>> obs_; //! Observation: { Frame-Id, Px-Measurement}
  bool in_ba_graph_ = false;

  Landmark(FrameId frame_id, const Point2& px)
  {
    obs_.push_back(std::make_pair(frame_id, px));
  }
};
using LandmarkTable = std::unordered_map<Key, Landmark>;

} // namespace duckie

