#pragma once

#include <memory>
#include <ros/ros.h>
#include <gtsam/geometry/Cal3_S2.h>
#include <gtsam/slam/SmartProjectionPoseFactor.h>

#include <duckie_vio/vio_types.h>

namespace duckie {

class Visualizer
{
public:
  using Ptr = std::shared_ptr<Visualizer>;
  using SmartFactor = gtsam::SmartProjectionPoseFactor<gtsam::Cal3_S2>;
  using SmartFactorMap = gtsam::FastMap<LandmarkId, std::pair<SmartFactor::shared_ptr, int>>;

  Visualizer();
  ~Visualizer() = default;

  ros::NodeHandle pnh_;
  ros::Publisher pub_markers_;
  double vis_scale_ = 1.2;

  void displayFrame(
      const Frame& frame, const LandmarkTable& landmarks);

  void visualizeFrames(
      const gtsam::Values& values);

  void visualizeSmartFactors(
      const SmartFactorMap& smart_factors,
      const gtsam::Values& values);

  void visualizeVelocity(
      const gtsam::Values& values);
};

} // namespace duckie
