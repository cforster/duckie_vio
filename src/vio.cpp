#include <duckie_vio/vio.h>

#include <gflags/gflags.h>
#include <glog/logging.h>

#include <boost/shared_ptr.hpp>        // used for opengv.
#include <opencv2/imgproc/imgproc.hpp> // good features to track
#include <opencv2/video/tracking.hpp>  // klt
#include <opencv2/highgui/highgui.hpp> // debug, show img.

#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/linear/linearExceptions.h>

#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/relative_pose/TranslationOnlySacProblem.hpp>
#include <opengv/sac_problems/relative_pose/CentralRelativePoseSacProblem.hpp>
#include <opengv/relative_pose/methods.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/triangulation/methods.hpp>

#include <duckie_vio/vio_types.h>
#include <duckie_vio/vio_visualizer.h>

DEFINE_double(vio_ransac_reprojection_err_thresh, 0.05,
              "RANSAC reprojection error threshold.");
DEFINE_double(vio_smart_reprojection_err_thresh, 1.5,
              "reprojection factor threshold [px]");
DEFINE_int32(vio_keyframe_rate, 5, "");
DEFINE_uint64(vio_max_landmarks_per_frame, 70, "");

namespace duckie {

//------------------------------------------------------------------------------
Vio::Vio(const boost::shared_ptr<gtsam::Cal3DS2>& cam,
         const Pose3& T_B_C)
  : viz_(std::make_shared<Visualizer>())
  , cam_(cam)
  , unitcam_(boost::make_shared<gtsam::Cal3_S2>())
  , T_B_C_(T_B_C)
  , imu_buffer_(2.0)
{
  // Smart projection factors.
  smart_noise_ = gtsam::noiseModel::Isotropic::Sigma(
        2, FLAGS_vio_smart_reprojection_err_thresh / cam_->fx());
  smart_factor_params_ = gtsam::SmartProjectionParams(
        gtsam::JACOBIAN_SVD, gtsam::IGNORE_DEGENERACY, false, true);
  smart_factor_params_.setRankTolerance(1e-7);
  smart_factor_params_.setLandmarkDistanceThreshold(30.0);
  smart_factor_params_.setDynamicOutlierRejectionThreshold(
        FLAGS_vio_smart_reprojection_err_thresh / cam_->fx());

  // Imu parameters.
  double gyro_noise_density = 0.000372; // Gyro noise density (sigma). [rad/s*1/sqrt(Hz)]
  double acc_noise_density = 0.00372; // Accelerometer noise density (sigma). [m/s^2*1/sqrt(Hz)]
  double imu_integration_sigma = 0.0; // IMU integration sigma (sigma). GTSAM preintegration option.
  double gyro_bias_random_walk_sigma = 0.00004; // Gyro bias random walk (sigma). [rad/s^2*1/sqrt(Hz)]
  double acc_bias_random_walk_sigma = 0.000866; // Accelerometer bias random walk (sigma). [m/s^3*1/sqrt(Hz)]
  double gravity_magnitude = 9.81007; // Norm of the Gravitational acceleration. [m/s^2]
  preintegration_params_ = PreintegratedImuMeasurements::Params::MakeSharedD(-gravity_magnitude);
  preintegration_params_->gyroscopeCovariance =
      std::pow(gyro_noise_density, 2.0) * Eigen::Matrix3d::Identity();
  preintegration_params_->accelerometerCovariance =
      std::pow(acc_noise_density, 2.0) * Eigen::Matrix3d::Identity();
  preintegration_params_->integrationCovariance =
      std::pow(imu_integration_sigma, 2.0) * Eigen::Matrix3d::Identity();
  preintegration_params_->biasAccCovariance =
      std::pow(acc_bias_random_walk_sigma, 2.0) * Eigen::Matrix3d::Identity();
  preintegration_params_->biasOmegaCovariance =
      std::pow(gyro_bias_random_walk_sigma, 2.0) * Eigen::Matrix3d::Identity();
  preintegration_params_->use2ndOrderCoriolis = false;

  // iSAM2
  //gtsam::ISAM2GaussNewtonParams gauss_newton_params;
  //gauss_newton_params.setWildfireThreshold(0.001);
  gtsam::ISAM2DoglegParams dogleg_params;
  dogleg_params.setVerbose(false); // only for debugging.
  gtsam::ISAM2Params isam_param;
  isam_param.optimizationParams = dogleg_params; //gauss_newton_params;
  isam_param.relinearizeThreshold = 0.1;
  isam_param.relinearizeSkip = 1;
  //isam_param.enableDetailedResults = true;   // only for debugging.
  isam_param.factorization = gtsam::ISAM2Params::QR;
  //isam_param.evaluateNonlinearError = true;  // only for debugging.
  isam_ = std::make_shared<gtsam::ISAM2>(isam_param);
}

//------------------------------------------------------------------------------
void Vio::addInertialMeasurement(
    const int64_t timestamp, const Vector3& accel, const Vector3& gyro)
{
  Vector6 imu_accgyr;
  imu_accgyr.head<3>() = accel;
  imu_accgyr.tail<3>() = gyro;
  imu_buffer_.insert(timestamp, imu_accgyr);
  synchronizeCameraImuAndProcessWhenReady();
}

//------------------------------------------------------------------------------
void Vio::addImage(
    const int64_t timestamp, const cv::Mat& img)
{
  last_img_stamp_ = new_img_stamp_;
  new_img_stamp_ = timestamp;
  new_img_ = img;
  synchronizeCameraImuAndProcessWhenReady();
}

//------------------------------------------------------------------------------
void Vio::synchronizeCameraImuAndProcessWhenReady()
{
  if(last_img_stamp_ == new_img_stamp_)
  {
    LOG_FIRST_N(WARNING, 3) << "Synchronization: Have no image yet.";
    return;
  }

  if(imu_buffer_.empty())
  {
    LOG_FIRST_N(WARNING, 3) << "Synchronization: Have no IMU measurements yet.";
    return;
  }

  if(std::get<1>(imu_buffer_.getOldestAndNewestStamp()) <= new_img_stamp_)
  {
    LOG_FIRST_N(WARNING, 3) << "Don't have all IMU measurements to process image.";
    return;
  }

  ImuStamps imu_stamps;
  ImuAccGyr imu_accgyr;
  std::tie(imu_stamps, imu_accgyr) = imu_buffer_.getBetweenValuesInterpolated(
        last_img_stamp_, new_img_stamp_);
  if (imu_stamps.size() == 0)
  {
    LOG(WARNING) << "Have no IMU measurements yet.";
    return;
  }

  // Process synchronized data.
  processSynchronizedData(new_img_stamp_, new_img_, imu_stamps, imu_accgyr);
  last_img_stamp_ = new_img_stamp_;
}

//------------------------------------------------------------------------------
void Vio::processSynchronizedData(
    const int64_t timestamp, const cv::Mat& img,
    const ImuStamps& imu_stamps, const ImuAccGyr& imu_accgyr)
{
  CHECK_GT(imu_accgyr.cols(), 0);

  VLOG(3) << "================= FRAME " << frame_count_ << " =================";
  frame_k_ = std::make_shared<Frame>(frame_count_, timestamp, img);

  if (frame_km1_ != nullptr)
  {
    // Feature tracking using KLT.
    featureTracking(*frame_km1_, *frame_k_);

    // Integrate imu measurements:
    integrateImuMeasurements(imu_stamps, imu_accgyr);

    // Select Keyframe
    if(frame_k_->id_ - frame_lkf_->id_ > FLAGS_vio_keyframe_rate)
    {
      frame_k_->is_keyframe_ = true;

      // If disparity is to small, we add a zero-velocity prior:
      double disparity = computeMedianDisparity(*frame_lkf_, *frame_k_);

      // Predict next pose using preintegrated imu measurements (pim):
      gtsam::NavState navstate_lkf(T_W_Blkf_, W_v_Blkf_);
      gtsam::NavState navstate_k = pim_->predict(navstate_lkf, imu_bias_lkf_);

      // Update state with initial guess:
      new_values_.insert(gtsam::Symbol('x', frame_k_->id_), navstate_k.pose());
      new_values_.insert(gtsam::Symbol('v', frame_k_->id_), navstate_k.velocity());
      new_values_.insert(gtsam::Symbol('b', frame_k_->id_), imu_bias_lkf_);

      // Add IMU measurements between last key-frame (lkf) and current frame (k).
      addImuFactor(frame_lkf_->id_, frame_k_->id_);

      if (disparity < 1.0)
      {
        // If we don't move, add zero velocity prior.
        addZeroVelocityPrior(frame_k_->id_);
        addNoMotionFactor(frame_lkf_->id_, frame_k_->id_);
      }
      else
      {
        // Outlier rejection using RANSAC.
        geometricOutlierRejection(*frame_lkf_, *frame_k_, pim_->deltaRij());

        // Add landmarks to graph.
        addLandmarksToGraph(*frame_k_);
      }

      // Run optimization.
      optimize(10);
    }
  }
  else
  {
    // First frame is keyframe:
    frame_k_->is_keyframe_ = true;

    // Update state with initial guess:
    new_values_.insert(gtsam::Symbol('x', frame_k_->id_), T_W_Blkf_);
    new_values_.insert(gtsam::Symbol('v', frame_k_->id_), W_v_Blkf_);
    new_values_.insert(gtsam::Symbol('b', frame_k_->id_), imu_bias_lkf_);

    // Initialize system:
    addInertialPriorFactors(frame_k_->id_, imu_accgyr);
  }


  if (frame_k_->is_keyframe_)
  {
    frame_lkf_ = frame_k_;

    // Detect new features.
    featureDetection(frame_km1_.get(), *frame_k_);

    // Restart IMU preintegration.
    pim_.reset();

    // Visualize.
    VLOG(3) << "Gyr. Bias = " << imu_bias_lkf_.gyroscope().transpose();
    VLOG(3) << "Acc. Bias = " << imu_bias_lkf_.accelerometer().transpose();
    viz_->visualizeFrames(state_);
    viz_->visualizeSmartFactors(smart_factors_map_, state_);
    viz_->visualizeVelocity(state_);
    viz_->displayFrame(*frame_k_, landmarks_);
  }

  // Prepare for next iteration.
  frame_km1_ = frame_k_;
  frame_k_.reset();
  ++frame_count_;
}

//------------------------------------------------------------------------------
void Vio::featureDetection(const Frame* ref_frame, Frame& cur_frame)
{
  // How many features do we need?
  int n_existing = 0;
  for(const LandmarkId lm_id : cur_frame.landmarks_)
  {
    if(lm_id != -1)
      ++n_existing;
  }
  const int need_n_corners = 200 - n_existing;
  if(need_n_corners <= 0)
    return;

  // Create mask such that new keypoints are not close to old ones.
  cv::Mat mask(cur_frame.img_.size(), CV_8UC1, cv::Scalar(255));
  if(ref_frame)
  {
    for(size_t i = 0; i < ref_frame->keypoints_.size(); ++i)
    {
      if(ref_frame->landmarks_.at(i) != -1)
      {
        const Point2& px = ref_frame->keypoints_.at(i);
        cv::circle(mask, cv::Point(px.x(), px.y()), 30, cv::Scalar(0), CV_FILLED);
      }
    }
  }

  const double quality_level = 0.01;
  const double min_distance = 20.0;
  const int block_size = 3;
  const bool use_harris_detector = false;
  const double k = 0.04;
  std::vector< cv::Point2f > corners;
  cv::goodFeaturesToTrack(cur_frame.img_, corners, need_n_corners, quality_level,
                          min_distance, mask, block_size, use_harris_detector, k);

  // Store features in our Frame and compute corresponding bearing vectors.
  cur_frame.keypoints_.reserve(cur_frame.keypoints_.size() + corners.size());
  cur_frame.landmarks_.reserve(cur_frame.keypoints_.size() + corners.size());
  for (const cv::Point2f& cv_px : corners)
  {
    const Point2 px(cv_px.x, cv_px.y);
    LandmarkId landmark_id_ = landmark_count_;
    cur_frame.keypoints_.push_back(px);
    cur_frame.landmarks_.push_back(landmark_id_);
    landmarks_.insert(std::make_pair(landmark_id_, Landmark(cur_frame.id_, px)));
    ++landmark_count_;
    //cv::circle(mask, cv_px, 5, cv::Scalar(100), CV_FILLED);
  }
  //cv::imshow("mask", mask);
  VLOG(3) << "Detected " << corners.size() << " features in frame " << cur_frame.id_;
}

//------------------------------------------------------------------------------
void Vio::featureTracking(Frame& ref_frame, Frame& cur_frame)
{
  // Pyramidal Lucas-Kanade Feature Tracking.
  const int klt_win_size = 24;
  const int klt_max_iter = 30;
  const double klt_eps = 0.001;
  std::vector<uchar> status;
  std::vector<float> error;
  cv::TermCriteria termcrit(cv::TermCriteria::COUNT+cv::TermCriteria::EPS,
                            klt_max_iter, klt_eps);
  std::vector<cv::Point2f> px_ref;
  std::vector<size_t> indices;
  indices.reserve(ref_frame.keypoints_.size());
  px_ref.reserve(ref_frame.keypoints_.size());
  for (size_t i = 0; i < ref_frame.keypoints_.size(); ++i)
  {
    if (ref_frame.landmarks_[i] != -1)
    {
      const Point2& pt = ref_frame.keypoints_[i];
      px_ref.push_back(cv::Point2f(pt.x(), pt.y()));
      indices.push_back(i);
    }
  }
  std::vector<cv::Point2f> px_cur = px_ref;
  CHECK_GT(px_cur.size(), 0u);
  cv::calcOpticalFlowPyrLK(ref_frame.img_, cur_frame.img_,
                           px_ref, px_cur,
                           status, error,
                           cv::Size2i(klt_win_size, klt_win_size),
                           4, termcrit, cv::OPTFLOW_USE_INITIAL_FLOW);

  CHECK(cur_frame.keypoints_.empty());
  cur_frame.landmarks_.reserve(px_ref.size());
  cur_frame.keypoints_.reserve(px_ref.size());
  for(size_t i = 0, n = 0; i < indices.size(); ++i)
  {
    size_t i_ref = indices[i];
    if(!status[i])
    {
      ref_frame.landmarks_[i_ref] = -1;
      continue;
    }
    cur_frame.landmarks_.push_back(ref_frame.landmarks_[i_ref]);
    const cv::Point2f& px = px_cur[i];
    cur_frame.keypoints_.push_back(Point2(px.x, px.y));
    ++n;
  }
  VLOG(3) << "Tracked " << cur_frame.keypoints_.size() << " features.";
}

//------------------------------------------------------------------------------
std::vector<std::pair<size_t, size_t>> Vio::findMatchingKeypoints(
    const Frame& ref_frame, const Frame& cur_frame)
{
  // Find keypoints that observe the same landmarks in both frames:
  std::map<LandmarkId, size_t> ref_lm_index_map;
  for(size_t i = 0; i < ref_frame.landmarks_.size(); ++i)
  {
    LandmarkId ref_id = ref_frame.landmarks_.at(i);
    if(ref_id != -1)
    {
      ref_lm_index_map[ref_id] = i;
    }
  }
  std::vector<std::pair<size_t, size_t>> matches_ref_cur;
  matches_ref_cur.reserve(ref_lm_index_map.size());
  for(size_t i = 0; i < cur_frame.landmarks_.size(); ++i)
  {
    LandmarkId cur_id = cur_frame.landmarks_.at(i);
    if(cur_id != -1)
    {
      auto it = ref_lm_index_map.find(cur_id);
      if(it != ref_lm_index_map.end())
      {
        matches_ref_cur.push_back(std::make_pair(it->second, i));
      }
    }
  }
  return matches_ref_cur;
}

//------------------------------------------------------------------------------
double Vio::computeMedianDisparity(
    const Frame& ref_frame, const Frame& cur_frame)
{
  // Find keypoints that observe the same landmarks in both frames:
  std::vector<std::pair<size_t, size_t>> matches_ref_cur =
      findMatchingKeypoints(ref_frame, cur_frame);

  // Compute disparity:
  std::vector<double> disparity;
  disparity.reserve(matches_ref_cur.size());
  for(const std::pair<size_t, size_t>& rc : matches_ref_cur)
  {
    disparity.push_back(
          (cur_frame.keypoints_[rc.second]
           - ref_frame.keypoints_[rc.first]).vector().norm());
  }

  if(disparity.empty())
  {
    LOG(WARNING) << "Have no matches for disparity computation.";
    return 0.0;
  }

  // Compute median:
  const size_t center = disparity.size() / 2;
  std::nth_element(disparity.begin(), disparity.begin() + center, disparity.end());
  return disparity[center];
}

//------------------------------------------------------------------------------
bool Vio::geometricOutlierRejection(
    Frame& ref_frame, Frame& cur_frame, const Rot3& R_ref_cur)
{
  // Find keypoints that observe the same landmarks in both frames:
  std::vector<std::pair<size_t, size_t>> matches_ref_cur =
      findMatchingKeypoints(ref_frame, cur_frame);

  // Create vector of bearing vectors.
  BearingVectors f_cur; f_cur.reserve(matches_ref_cur.size());
  BearingVectors f_ref; f_cur.reserve(matches_ref_cur.size());
  for (const std::pair<size_t, size_t>& it : matches_ref_cur)
  {
    Vector3 f;
    f << cam_->calibrate(ref_frame.keypoints_.at(it.first), 1e-2).vector(), 1.0;
    f_ref.push_back(f.normalized());
    f << cam_->calibrate(cur_frame.keypoints_.at(it.second), 1e-2).vector(), 1.0;
    f_cur.push_back(f.normalized());
  }

  // Setup problem.
  using Problem = opengv::sac_problems::relative_pose::CentralRelativePoseSacProblem; // 5-point
  //using Problem = opengv::sac_problems::relative_pose::TranslationOnlySacProblem; // 2-point
  using Adapter = opengv::relative_pose::CentralRelativeAdapter;
  Adapter adapter(f_ref, f_cur);//, R_ref_cur.matrix()); // rotation only needed for 2-point
  boost::shared_ptr<Problem> problem(new Problem(adapter, Problem::NISTER));
  opengv::sac::Ransac<Problem> ransac;
  ransac.sac_model_ = problem;
  ransac.threshold_ =
      1.0 - std::cos(std::atan(std::sqrt(2) / cam_->fx()) * FLAGS_vio_ransac_reprojection_err_thresh);
  ransac.max_iterations_ = 100;
  ransac.probability_ = 0.995;

  // Solve.
  const int verbosity_level = 0;
  if (!ransac.computeModel(verbosity_level))
  {
    LOG(WARNING) << "5Pt RANSAC could not find a solution";
    return false;
  }
  VLOG(3) << "RANSAC:"
          << ", #iter = " << ransac.iterations_
          << ", #inliers = " << ransac.inliers_.size();

  // Get outlier indices from inlier indices.
  std::sort(ransac.inliers_.begin(), ransac.inliers_.end(), std::less<int>());
  std::vector<int> outliers;
  outliers.reserve(matches_ref_cur.size() - ransac.inliers_.size());
  size_t k = 0;
  for (size_t i = 0u; i < matches_ref_cur.size(); ++i)
  {
    if (k < ransac.inliers_.size() && ransac.inliers_[k] < static_cast<int>(i))
      ++k;
    if (k >= matches_ref_cur.size() || ransac.inliers_[k] != static_cast<int>(i))
      outliers.push_back(i);
  }
  VLOG_IF(3, !outliers.empty()) << "RANSAC has " << outliers.size() << " outliers.";

  // Remove outliers.
  for (const int i : outliers)
  {
    ref_frame.landmarks_.at(matches_ref_cur[i].first) = -1;
    cur_frame.landmarks_.at(matches_ref_cur[i].second) = -1;
  }

  // Update landmarks with inlier observations.
  for (size_t i = 0; i < cur_frame.landmarks_.size(); ++i)
  {
    if (cur_frame.landmarks_[i] != -1)
    {
      auto it = landmarks_.find(cur_frame.landmarks_[i]);
      if (it != landmarks_.end())
      {
        it->second.obs_.push_back(std::make_pair(cur_frame.id_,
                                                 cur_frame.keypoints_[i]));
      }
    }
  }

  return ransac.inliers_.size() > 10;
}

//------------------------------------------------------------------------------
void Vio::addLandmarksToGraph(const Frame& frame)
{
  // Find N visible landmarks with most observations.
  std::vector<std::pair<size_t, int>> lm_obs_count;
  for (size_t i = 0; i < frame.landmarks_.size(); ++i)
  {
    LandmarkId lm_id = frame.landmarks_.at(i);
    if (lm_id == -1)
    {
      continue;
    }

    auto lm_it = landmarks_.find(lm_id);
    if (lm_it == landmarks_.end())
    {
      LOG(WARNING) << "Landmark " << lm_id << " not in table.";
      continue;

    }

    // If landmark is already in graph, check if it is marked as degenerate:
    // TODO: make this more efficient, maybe add a flag in the landmark or
    //       completely get rid of landmarks and accumulte observations directly
    //       in smart factors.
    const Landmark& lm = lm_it->second;
    if (lm.in_ba_graph_)
    {
      auto sf_it = smart_factors_map_.find(frame.id_);
      if (sf_it != smart_factors_map_.end())
      {
        if (sf_it->second.first->isDegenerate())
        {
          continue;
        }
      }
    }

    lm_obs_count.push_back(std::make_pair(i, lm.obs_.size()));
  }

  size_t max_landmarks = FLAGS_vio_max_landmarks_per_frame;
  if (lm_obs_count.size() > max_landmarks)
  {
    std::sort(lm_obs_count.begin(), lm_obs_count.end(),
              [](const std::pair<size_t, int>& lhs, const std::pair<size_t, int>& rhs)
              { return lhs.second > rhs.second; });
    lm_obs_count.erase(lm_obs_count.begin() + max_landmarks, lm_obs_count.end());
  }

  // Add selected landmarks to graph.
  int n_new_landmarks = 0;
  int n_updated_landmarks = 0;
  for (const std::pair<size_t, int>& it : lm_obs_count)
  {
    size_t i = it.first;
    LandmarkId lm_id = frame.landmarks_.at(i);
    Landmark& lm = landmarks_.at(lm_id);
    if(!lm.in_ba_graph_)
    {
      addLandmarkToGraph(frame.landmarks_.at(i), lm);
      ++n_new_landmarks;
    }
    else
    {
      updateLandmarkInGraph(lm_id, lm, std::make_pair(frame.id_, frame.keypoints_.at(i)));
      ++n_updated_landmarks;
    }
  }
  VLOG(40) << "Added " << n_new_landmarks << " new landmarks";
  VLOG(40) << "Updated " << n_updated_landmarks << " landmarks in graph.";
}

//------------------------------------------------------------------------------
void Vio::addLandmarkToGraph(LandmarkId lm_id, Landmark& lm)
{
  CHECK(!lm.in_ba_graph_);
  lm.in_ba_graph_ = true;

  // We use a unit pinhole projection camera for the smart factors to be more efficient.
  SmartFactor::shared_ptr new_factor(
        new SmartFactor(smart_noise_, unitcam_, T_B_C_, smart_factor_params_));

  // add observations to smart-factor
  for (const std::pair<LandmarkId,Point2>& obs : lm.obs_)
  {
    Point2 px = cam_->calibrate(obs.second, 1e-2); // TODO: Euroc calibration can only deal with very low thresholds.
    new_factor->add(px, gtsam::Symbol('x', obs.first));
  }

  // add new factor: Todo, add to optimization problem.
  new_smart_factors_.insert(std::make_pair(lm_id, new_factor));
  smart_factors_map_.insert(std::make_pair(lm_id, std::make_pair(new_factor, -1)));

  VLOG(100) << "Added new smart landmark for point: " << lm_id;
}

//------------------------------------------------------------------------------
void Vio::updateLandmarkInGraph(const LandmarkId lm_id, const Landmark& lm,
                                const std::pair<FrameId, Point2>& obs)
{
  CHECK(lm.in_ba_graph_);

  // Update existing smart-factor.
  auto smart_factors_map_it = smart_factors_map_.find(lm_id);
  CHECK(smart_factors_map_it != smart_factors_map_.end())
      << "Tried to add a new observation to an existing landmark that is not in graph";

  SmartFactor::shared_ptr old_factor = smart_factors_map_it->second.first;
  SmartFactor::shared_ptr new_factor = boost::make_shared<SmartFactor>(*old_factor);
  Point2 px = cam_->calibrate(obs.second, 1e-2);  // TODO: Euroc calibration can only deal with very low thresholds.
  new_factor->add(px, gtsam::Symbol('x', obs.first));

  // update the factor
  if (smart_factors_map_it->second.second != -1)
  {
    // if slot is still -1, it means that the factor is not inserted yet in the graph
    new_smart_factors_.insert(std::make_pair(lm_id, new_factor));
  }
  smart_factors_map_it->second.first = new_factor;

  VLOG(100) << "Added smart observation to point: " << lm_id;
}

//------------------------------------------------------------------------------
void Vio::integrateImuMeasurements(
    const ImuStamps& imu_stamps, const ImuAccGyr& imu_accgyr)
{
  CHECK_GE(imu_stamps.size(), 2);

  if (!pim_)
  {
    pim_ = std::make_shared<PreintegratedImuMeasurements>(
          preintegration_params_, imu_bias_lkf_);
  }

  for (int i = 0; i < imu_stamps.size()-1; ++i)
  {
    Vector3 measured_acc = imu_accgyr.block<3,1>(0,i);
    Vector3 measured_omega = imu_accgyr.block<3,1>(3,i);
    double delta_t = static_cast<double>(imu_stamps(i+1) - imu_stamps(i)) / 1e9;
    pim_->integrateMeasurement(measured_acc, measured_omega, delta_t);
  }
}

//------------------------------------------------------------------------------
void Vio::addImuFactor(
    const FrameId& from_id, const FrameId& to_id)
{
  new_factors_.push_back(
        boost::make_shared<gtsam::CombinedImuFactor>(
          gtsam::Symbol('x', from_id), gtsam::Symbol('v', from_id),
          gtsam::Symbol('x', to_id), gtsam::Symbol('v', to_id),
          gtsam::Symbol('b', from_id),
          gtsam::Symbol('b', to_id),
          *pim_));
}

//------------------------------------------------------------------------------
void Vio::addInertialPriorFactors(
    const FrameId& frame_id,
    const ImuAccGyr& imu_accgyr)
{
  // Set initial coordinate frame based on gravity direction.
  // This only works if the sensor is initially not moving and the accelerometer
  // has zero bias (which is never the case..).
  const Vector3 g = imu_accgyr.topRows<3>().rowwise().sum();
  const Vector3 z = g.normalized();
  VLOG(1) << "Initial gravity direction = " << z.transpose();
  Vector3 p(1,0,0);
  Vector3 p_alternative(0,1,0);
  if(std::abs(z.dot(p)) > std::abs(z.dot(p_alternative)))
    p = p_alternative;
  Vector3 y = z.cross(p); // make sure gravity is not in x direction
  y.normalize();
  const Vector3 x = y.cross(z);
  Matrix3 R_B_W; // world unit vectors in imu coordinates
  R_B_W.col(0) = x;
  R_B_W.col(1) = y;
  R_B_W.col(2) = z;
  T_W_Blkf_ = Pose3(Rot3(R_B_W.transpose()), Vector3::Zero());

  // Set initial pose uncertainty: constrain mainly position and global yaw.
  // roll and pitch is observable, therefore low variance.
  double init_pos_sigma = 0.00001;
  double init_roll_pitch_sigma = 10.0 / 180.0 * M_PI;
  double init_yaw_sigma = 0.1 / 180.0 * M_PI;
  Matrix6 pose_prior_covariance = Matrix6::Zero();
  pose_prior_covariance.diagonal()[0] = init_roll_pitch_sigma * init_roll_pitch_sigma;
  pose_prior_covariance.diagonal()[1] = init_roll_pitch_sigma * init_roll_pitch_sigma;
  pose_prior_covariance.diagonal()[2] = init_yaw_sigma * init_yaw_sigma;
  pose_prior_covariance.diagonal()[3] = init_pos_sigma * init_pos_sigma;
  pose_prior_covariance.diagonal()[4] = init_pos_sigma * init_pos_sigma;
  pose_prior_covariance.diagonal()[5] = init_pos_sigma * init_pos_sigma;

  // Rotate initial uncertainty into local frame, where the uncertainty is specified.
  pose_prior_covariance.topLeftCorner(3,3) =
      R_B_W * pose_prior_covariance.topLeftCorner(3,3) * R_B_W.transpose();

  // Add pose prior.
  gtsam::SharedNoiseModel noise_init_pose =
      gtsam::noiseModel::Gaussian::Covariance(pose_prior_covariance);
  new_factors_.push_back(
        boost::make_shared<gtsam::PriorFactor<gtsam::Pose3> >(
          gtsam::Symbol('x', frame_id), T_W_Blkf_, noise_init_pose));

  // Add initial velocity priors.
  double velocity_sigma = 0.001; // We start in static position.
  gtsam::SharedNoiseModel velocity_prior_noise =
      gtsam::noiseModel::Isotropic::Sigma(3, velocity_sigma);
  new_factors_.push_back(
        boost::make_shared<gtsam::PriorFactor<gtsam::Vector3>>(
          gtsam::Symbol('v', frame_id), W_v_Blkf_, velocity_prior_noise));

  // Add initial bias priors:
  double acc_bias_sigma = 0.001;
  double omega_bias_sigma = 0.2;
  Vector6 prior_bias_sigmas;
  prior_bias_sigmas.head<3>().setConstant(acc_bias_sigma);
  prior_bias_sigmas.tail<3>().setConstant(omega_bias_sigma);
  gtsam::SharedNoiseModel imu_bias_prior_noise =
      gtsam::noiseModel::Diagonal::Sigmas(prior_bias_sigmas);
  new_factors_.push_back(
        boost::make_shared<gtsam::PriorFactor<gtsam::imuBias::ConstantBias>>(
          gtsam::Symbol('b', frame_id), imu_bias_lkf_, imu_bias_prior_noise));

  VLOG(40) << "Added inertial priors for frame " << frame_id;
}

//------------------------------------------------------------------------------
void Vio::addZeroVelocityPrior(
    const FrameId& frame_id)
{
  double velocity_sigma = 0.001; // We start in static position.
  gtsam::SharedNoiseModel velocity_prior_noise =
      gtsam::noiseModel::Isotropic::Sigma(3, velocity_sigma);
  new_factors_.push_back(
        boost::make_shared<gtsam::PriorFactor<gtsam::Vector3>>(
          gtsam::Symbol('v', frame_id), Vector3::Zero(), velocity_prior_noise));
  LOG(WARNING) << "No motion detected, adding zero velocity prior.";
}

//------------------------------------------------------------------------------
void Vio::addNoMotionFactor(const FrameId& from_id, const FrameId& to_id)
{
  double pos_sigma = 0.0001;
  double rot_sigma = 0.00001;
  Vector6 sigmas;
  sigmas.head<3>().setConstant(rot_sigma);
  sigmas.tail<3>().setConstant(pos_sigma);
  gtsam::SharedNoiseModel noise = gtsam::noiseModel::Diagonal::Sigmas(sigmas);
  new_factors_.push_back(
        boost::make_shared<gtsam::BetweenFactor<gtsam::Pose3>>(
          gtsam::Symbol('x', from_id), gtsam::Symbol('x', to_id), Pose3(), noise));
  LOG(WARNING) << "No motion detected, adding no relative motion prior.";
}

//------------------------------------------------------------------------------
void Vio::optimize(int max_iterations)
{
  CHECK_NOTNULL(isam_.get());

  // We need to remove all smart factors that have new observations.
  std::vector<size_t> delete_slots;
  std::vector<Key> smart_factor_keys;
  gtsam::NonlinearFactorGraph new_factors;
  for (auto& s : new_smart_factors_)
  {
    new_factors.push_back(s.second);
    smart_factor_keys.push_back(s.first);
    const auto& it = smart_factors_map_.find(s.first);
    if(it->second.second != -1) // get current slot
      delete_slots.push_back(it->second.second);
  }
  new_factors.push_back(new_factors_.begin(), new_factors_.end());
  new_factors_.resize(0);

  // Compute iSAM update.
  gtsam::ISAM2Result result;
  int n_iter = 0;
  VLOG(3) << "iSAM2 update with " << new_factors.size() << " graph updates"
          << " and " << new_values_.size() << " new values "
          << " and " << delete_slots.size() << " delete indices";

  if (false)
  {
    new_factors.print();
    new_values_.print();
  }

  try
  {
    ++n_iter;
    result = isam_->update(new_factors, new_values_, delete_slots);

    new_smart_factors_.clear();
    new_values_.clear();
  }
  catch (const gtsam::IndeterminantLinearSystemException& e)
  {
    std::cerr << e.what() << std::endl;
    gtsam::Key var = e.nearbyVariable();
    gtsam::Symbol symb(var);
    LOG(ERROR) << "Variable has type '" << symb.chr() << "' "
               << "and index " << symb.index() << std::endl;
    throw;
  }

  // update slots of new inserted indices:
  for (size_t i = 0; i < smart_factor_keys.size(); ++i)
  {
    const auto& it = smart_factors_map_.find(smart_factor_keys.at(i));
    it->second.second = result.newFactorsIndices.at(i);
  }

  // Do some more iterations.
  for  (; n_iter < max_iterations; ++n_iter)
  {
    try
    {
      result = isam_->update();
    }
    catch(const gtsam::IndeterminantLinearSystemException& e)
    {
      LOG(ERROR) << e.what();
      gtsam::Key var = e.nearbyVariable();
      gtsam::Symbol symb(var);
      LOG(ERROR) << "Variable has type '" << symb.chr() << "' "
                 << "and index " << symb.index();
      throw;
    }
  }

  // Get states we need for next iteration
  state_ = isam_->calculateEstimate();
  T_W_Blkf_ = state_.at<Pose3>(gtsam::Symbol('x', frame_k_->id_));
  W_v_Blkf_ = state_.at<Vector3>(gtsam::Symbol('v', frame_k_->id_));
  imu_bias_lkf_ = state_.at<gtsam::imuBias::ConstantBias>(gtsam::Symbol('b', frame_k_->id_));
  //VLOG(10) << "Final Error = " << isam_->getFactorsUnsafe().error(state_);
}

} // namespace duckie
