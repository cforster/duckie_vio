#include <duckie_vio/vio_imu_buffer.h>

namespace duckie {

std::tuple<int64_t, int64_t, bool> ImuBuffer::getOldestAndNewestStamp() const
{
  std::lock_guard<std::mutex> lock(mutex_);
  if(buffer_.empty())
  {
    return std::make_tuple(-1, -1, false);
  }
  return std::make_tuple(buffer_.begin()->first, buffer_.rbegin()->first, true);
}

std::pair<ImuStamps, ImuAccGyr>
ImuBuffer::getBetweenValuesInterpolated(int64_t stamp_from, int64_t stamp_to)
{
  CHECK_GE(stamp_from, 0);
  CHECK_LT(stamp_from, stamp_to);
  ImuStamps imu_stamps;
  ImuAccGyr imu_accgyr;

  std::lock_guard<std::mutex> lock(mutex_);
  if(buffer_.size() < 2)
  {
    LOG(WARNING) << "Buffer has less than 2 entries.";
    return std::make_pair(imu_stamps, imu_accgyr); // return empty means unsuccessful.
  }

  const int64_t oldest_stamp = buffer_.begin()->first;
  const int64_t newest_stamp = buffer_.rbegin()->first;
  if(stamp_from < oldest_stamp)
  {
    LOG(WARNING) << "Requests older timestamp than in buffer.";
    return std::make_pair(imu_stamps, imu_accgyr); // return empty means unsuccessful.
  }
  if(stamp_to > newest_stamp)
  {
    LOG(WARNING) << "Requests newer timestamp than in buffer.";
    return std::make_pair(imu_stamps, imu_accgyr); // return empty means unsuccessful.
  }

  auto it_from_before = iterator_equal_or_before(stamp_from);
  auto it_to_after = iterator_equal_or_after(stamp_to);
  CHECK(it_from_before != buffer_.end());
  CHECK(it_to_after != buffer_.end());
  auto it_from_after = it_from_before;
  ++it_from_after;
  auto it_to_before = it_to_after;
  --it_to_before;
  if(it_from_after == it_to_before)
  {
    LOG(WARNING) << "Not enough data for interpolation";
    return std::make_pair(imu_stamps, imu_accgyr); // return empty means unsuccessful.
  }

  // Count number of measurements.
  size_t n = 0;
  auto it = it_from_after;
  while(it != it_to_after)
  {
    ++n;
    ++it;
  }
  n += 2;

  // Interpolate values at start and end and copy in output vector.
  imu_stamps.resize(n);
  imu_accgyr.resize(6, n);
  for(size_t i = 0; i < n; ++i)
  {
    if(i == 0)
    {
      imu_stamps(i) = stamp_from;
      const double w =
          static_cast<double>(stamp_from - it_from_before->first) /
          static_cast<double>(it_from_after->first - it_from_before->first);
      imu_accgyr.col(i) = (1.0 - w) * it_from_before->second + w * it_from_after->second;
    }
    else if(i == n-1)
    {
      imu_stamps(i) = stamp_to;
      const double w =
          static_cast<double>(stamp_to - it_to_before->first) /
          static_cast<double>(it_to_after->first - it_to_before->first);
      imu_accgyr.col(i) = (1.0 - w) * it_to_before->second + w * it_to_after->second;
    }
    else
    {
      imu_stamps(i) = it_from_after->first;
      imu_accgyr.col(i) = it_from_after->second;
      ++it_from_after;
    }
  }

  return std::make_pair(imu_stamps, imu_accgyr);
}

typename ImuBuffer::Data::iterator ImuBuffer::iterator_equal_or_before(int64_t stamp)
{
  CHECK(!mutex_.try_lock()) << "Call lock() before accessing data.";
  auto it = buffer_.lower_bound(stamp);

  if(it->first == stamp)
  {
    return it; // Return iterator to key if exact key exists.
  }
  if(stamp > buffer_.rbegin()->first)
  {
    return (--buffer_.end()); // Pointer to last value.
  }
  if(it == buffer_.begin())
  {
    return buffer_.end(); // Invalid if data before first value.
  }
  --it;
  return it;
}

typename ImuBuffer::Data::iterator ImuBuffer::iterator_equal_or_after(int64_t stamp)
{
  CHECK(!mutex_.try_lock()) << "Call lock() before accessing data.";
  return buffer_.lower_bound(stamp);
}

} // namespace duckie
