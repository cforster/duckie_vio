#include <glog/logging.h>
#include <gflags/gflags.h>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <rosbag/query.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>

#include <gtsam/geometry/Cal3DS2.h>

#include <duckie_vio/vio.h>

DEFINE_string(bag_filename, "bagfile.bag", "Name of bagfile in data_dir.");
DEFINE_int32(skip_frames, 0, "Skip number of frames at the beginning.");

int main(int argc, char** argv)
{
  // Init glog & gflags.
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  google::InstallFailureSignalHandler();
  FLAGS_alsologtostderr = true;
  FLAGS_colorlogtostderr = true;
  FLAGS_v = 40;
  ros::init(argc, argv, "duckie_vio");

  // Create camera. (This is the Euroc Calibration)
  auto cam = boost::make_shared<gtsam::Cal3DS2>( // gtsam needs boost :/
        458.6548807207614, 457.2966964634893, 0.0,       // fx, fy, s
        367.2158039615726, 248.37534060980727,           // cx, cy
        -0.28340811217029355, 0.07395907389290132,       // radial distortion kp
        0.00019359502856909603, 1.7618711454538528e-05); // tangential distortion

  gtsam::Matrix4 T;
  T << 0.0148655429818, -0.999880929698, 0.004140296794, -0.021640145497,
        0.999557249008, 0.0149672133247, 0.025715529948, -0.064676986768,
      -0.0257744366974, 0.0037561883579, 0.999660727178, 0.0098107305894,
                   0.0,             0.0,            0.0,             1.0;
  gtsam::Pose3 T_B_C(T);

  // Create VIO
  auto vio = std::make_shared<duckie::Vio>(cam, T_B_C);

  // Load rosbag.
  rosbag::Bag rosbag;
  try
  {
    VLOG(1) << "Opening rosbag...";
    rosbag.open(FLAGS_bag_filename, rosbag::bagmode::Read);
    VLOG(1) << "done.";
  }
  catch(const std::exception e)
  {
    LOG(FATAL) << "Could not open rosbag " << FLAGS_bag_filename << ": " << e.what();
  }

  std::string img_topic = "/cam0/image_raw";
  std::string imu_topic = "/imu0";
  std::vector<std::string> topics { img_topic, imu_topic };
  rosbag::View bag_view(rosbag, rosbag::TopicQuery(topics));

  // Process rosbag.
  int n_frames = 0;
  for (const rosbag::MessageInstance& m : bag_view)
  {
    sensor_msgs::ImageConstPtr m_img = m.instantiate<sensor_msgs::Image>();
    if(m_img)
    {
      if(m.getTopic() == img_topic)
      {
        ++n_frames;
        if(n_frames < FLAGS_skip_frames)
        {
          continue;
        }

        // Add Image measurement.
        cv::Mat img;
        try
        {
          img = cv_bridge::toCvCopy(m_img, "mono8")->image;
        }
        catch (cv_bridge::Exception& e)
        {
          LOG(FATAL) << "Could not read image: " << e.what();
        }

        std::int64_t timestamp = m_img->header.stamp.toNSec();
        vio->addImage(timestamp, img);
      }
    }

    const sensor_msgs::ImuConstPtr m_imu = m.instantiate<sensor_msgs::Imu>();
    if(m_imu)
    {
      if(m.getTopic() == imu_topic)
      {
        // Add IMU Measurement.
        const Eigen::Vector3d gyr(
              m_imu->angular_velocity.x,
              m_imu->angular_velocity.y,
              m_imu->angular_velocity.z);
        const Eigen::Vector3d acc(
              m_imu->linear_acceleration.x,
              m_imu->linear_acceleration.y,
              m_imu->linear_acceleration.z);
        std::int64_t stamp = m_imu->header.stamp.toNSec();
        vio->addInertialMeasurement(stamp, acc, gyr);
      }
    }
  }
  return 0;
}
