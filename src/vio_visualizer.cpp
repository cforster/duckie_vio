#include <duckie_vio/vio_visualizer.h>

#include <glog/logging.h>
#include <gflags/gflags.h>
#include <opencv2/opencv.hpp>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

DEFINE_int32(vio_viz_sleep, 1, "sleep");

namespace duckie {

Visualizer::Visualizer()
  : pnh_("~")
{
  pub_markers_ = pnh_.advertise<visualization_msgs::Marker>("markers", 100);
}

void Visualizer::displayFrame(const Frame& frame, const LandmarkTable& landmarks)
{
  cv::Mat img_rgb = cv::Mat(frame.img_.size(), CV_8UC3);
  cv::cvtColor(frame.img_, img_rgb, cv::COLOR_GRAY2RGB);
  for (size_t i = 0; i < frame.keypoints_.size(); ++i)
  {
    Point2 px = frame.keypoints_.at(i);
    if (frame.landmarks_.at(i) == -1)
    {
      // Untracked landmarks are red.
      cv::circle(img_rgb, cv::Point2f(px.x(), px.y()), 4, cv::Scalar(0,0,255), 2);
    }
    else
    {
      // Draw feature track.
      auto it = landmarks.find(frame.landmarks_.at(i));
      if (it != landmarks.end())
      {
        const Landmark& lm = it->second;
        if (lm.obs_.size() > 1u)
        {
          // Tracked landmarks are green.
          cv::circle(img_rgb, cv::Point2f(px.x(), px.y()), 6, cv::Scalar(0,255,0), 1);
          for (auto obs = lm.obs_.rbegin(); obs != lm.obs_.rend(); ++obs)
          {
            cv::line(img_rgb,
                   cv::Point2f(px.x(), px.y()),
                   cv::Point2f(obs->second.x(), obs->second.y()), cv::Scalar(0,255,0), 1);
            px = obs->second;
          }
        }
        else
        {
          // New feature tracks are orange.
          cv::circle(img_rgb, cv::Point2f(px.x(), px.y()), 6, cv::Scalar(0,100,255), 1);
        }
      }
      else
      {
        LOG(WARNING) << "could not find landmark " << frame.landmarks_.at(i) << " in table."; 
      }
    }
  }

  cv::imshow("img", img_rgb);
  cv::waitKey(FLAGS_vio_viz_sleep);
}

inline geometry_msgs::Point getRosPoint(const Eigen::Ref<const Vector3>& point)
{
  geometry_msgs::Point p;
  p.x = point(0);
  p.y = point(1);
  p.z = point(2);
  return p;
}

void Visualizer::visualizeFrames(const gtsam::Values& values)
{
  if(pub_markers_.getNumSubscribers() == 0)
    return;

  visualization_msgs::Marker m;
  m.header.frame_id = "map";
  m.header.stamp = ros::Time::now();
  m.ns = "frames";
  m.id = 0;
  m.type = visualization_msgs::Marker::LINE_LIST;
  m.action = 0; // 0 = add/modify
  m.scale.x = vis_scale_ * 0.05;
  double length = 0.3 * vis_scale_;
  gtsam::Values::ConstFiltered<gtsam::Pose3> frames =
      values.filter<gtsam::Pose3>(gtsam::Symbol::ChrTest('x'));
  m.points.reserve(frames.size());
  std_msgs::ColorRGBA red;
  red.r = 1; red.g = 0; red.b = 0; red.a = 1;
  std_msgs::ColorRGBA green;
  green.r = 0; green.g = 1; green.b = 0; green.a = 1;
  std_msgs::ColorRGBA blue;
  blue.r = 0; blue.g = 0; blue.b = 1; blue.a = 1;
  m.colors.reserve(frames.size() * 3);
  m.points.reserve(frames.size() * 3);
  for(auto it = frames.begin(); it != frames.end(); ++it)
  {
    const Vector3& p = it->value.translation().vector();
    const Matrix3 R = it->value.rotation().matrix();
    m.points.push_back(getRosPoint(p));
    m.colors.push_back(red);
    m.points.push_back(getRosPoint(p + R.col(0) * length));
    m.colors.push_back(red);
    m.points.push_back(getRosPoint(p));
    m.colors.push_back(green);
    m.points.push_back(getRosPoint(p + R.col(1) * length));
    m.colors.push_back(green);
    m.points.push_back(getRosPoint(p));
    m.colors.push_back(blue);
    m.points.push_back(getRosPoint(p + R.col(2) * length));
    m.colors.push_back(blue);
  }
  pub_markers_.publish(m);
  VLOG(30) << "Published " << frames.size() <<  " frames.";
}

void Visualizer::visualizeSmartFactors(
    const SmartFactorMap& smart_factors,
    const gtsam::Values& values)
{
  if(pub_markers_.getNumSubscribers() == 0)
    return;

  double marker_scale = 0.03 * vis_scale_;
  visualization_msgs::Marker m;
  m.header.frame_id = "map";
  m.header.stamp = ros::Time();
  m.ns = "smart_points";
  m.id = 0;
  m.type = visualization_msgs::Marker::POINTS;
  m.action = 0; // add/modify
  m.scale.x = marker_scale;
  m.scale.y = marker_scale;
  m.scale.z = marker_scale;
  m.color.a = 1.0;
  m.color.r = 0.0;
  m.color.g = 0.7;
  m.color.b = 0.0;
  m.points.reserve(smart_factors.size());
  size_t n_degenerate = 0;
  size_t n_cheirality = 0;
  size_t n_zero = 0;
  for(const auto& sf : smart_factors)
  {
    Vector3 xyz = sf.second.first->point(values).get().vector();
    if (xyz.norm() < 0.00001)
    {
      ++n_zero;
      continue;
    }
    if(!sf.second.first->isDegenerate())
    {
      geometry_msgs::Point p;
      p.x = xyz.x();
      p.y = xyz.y();
      p.z = xyz.z();
      m.points.push_back(p);
    }
    else
    {
      ++n_degenerate;
      if(sf.second.first->isPointBehindCamera())
        ++n_cheirality;
    }
  }
  pub_markers_.publish(m);

  VLOG(30) << "Viz: Published " << m.points.size() << " smart-factors.";
  LOG_IF(WARNING, n_zero) << "Viz: Number of zero smart-factors:" << n_zero;
  LOG_IF(WARNING, n_degenerate) << "Viz: Number of degenerate smart-factors:" << n_degenerate;
  LOG_IF(WARNING, n_cheirality) << "Viz: Number of points behind camera smart-factors:" << n_cheirality;
}

void Visualizer::visualizeVelocity(const gtsam::Values& values)
{
  if(pub_markers_.getNumSubscribers() == 0)
    return;

  double marker_scale = 0.02*vis_scale_; // 0.02
  double velocity_scale = 0.1*vis_scale_; // 0.1
  visualization_msgs::Marker m;
  m.header.frame_id = "map";
  m.header.stamp = ros::Time();
  m.ns = "velocity";
  m.id = 0;
  m.type = visualization_msgs::Marker::LINE_LIST;
  m.action = 0; // add/modify
  m.scale.x = marker_scale;
  m.scale.y = marker_scale;
  m.scale.z = marker_scale;
  m.color.a = 1.0;
  m.color.r = 0.0;
  m.color.g = 0.0;
  m.color.b = 1.0;
  gtsam::Values::ConstFiltered<gtsam::Pose3> frames =
      values.filter<gtsam::Pose3>(gtsam::Symbol::ChrTest('x'));
  const size_t n_frames = frames.size();
  m.points.reserve(n_frames);
  for(auto it = frames.begin(); it != frames.end(); ++it)
  {
    const gtsam::Pose3& T_world_imu = it->value;
    const gtsam::Point3& xyz = T_world_imu.translation();
    geometry_msgs::Point p;
    p.x = xyz.x();
    p.y = xyz.y();
    p.z = xyz.z();
    m.points.push_back(p);
    const gtsam::Symbol frame_symbol(it->key);
    const gtsam::Symbol vel_symbol('v', frame_symbol.index());
    const gtsam::Vector3 vel = values.at< gtsam::Vector3 >(vel_symbol);
    const gtsam::Vector3 xyz_plus = xyz.vector()+vel*velocity_scale;

    p.x = xyz_plus.x();
    p.y = xyz_plus.y();
    p.z = xyz_plus.z();
    m.points.push_back(p);
  }
  pub_markers_.publish(m);
  VLOG(30) << "Viz: Published " << m.points.size()/2 << " velocities.";
}

} // namespace duckie
